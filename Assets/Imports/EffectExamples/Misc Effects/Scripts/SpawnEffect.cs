﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEffect : MonoBehaviour {

    public float spawnEffectTime = 2;
    public float pause = 1;
    public AnimationCurve fadeIn;

    public bool spawnImmediate = false;

    ParticleSystem ps;
    float timer = 0;
    Renderer _renderer;
    public bool playEffect;

    int shaderProperty;

	void Start ()
    {
        shaderProperty = Shader.PropertyToID("_cutoff");
        _renderer = GetComponent<Renderer>();
        ps = GetComponentInChildren <ParticleSystem>();

        var main = ps.main; 
        main.duration = spawnEffectTime;

        if(spawnImmediate) ps.Play();
    }
	
	void Update ()
    {
        if (playEffect)
        {
            if (timer > spawnEffectTime + pause)
            {
                ps.Play();
                playEffect = false;
                transform.GetChild(0).parent = null;
                Destroy(gameObject);
            }
            
            timer += Time.deltaTime;
            
            _renderer.material.SetFloat(shaderProperty, fadeIn.Evaluate(Mathf.InverseLerp(0, spawnEffectTime, timer)));
        }
    }

    public void Destroy()
    {
        playEffect = true;
    }
    
}
