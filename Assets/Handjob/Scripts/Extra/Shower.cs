﻿using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Extra
{
    public class Shower : MonoBehaviour
    {

        private VRTK_InteractableObject _interactable;
        private GameObject shower;
    
        private void Start()
        {
            _interactable = GetComponent<VRTK_InteractableObject>();
            _interactable.SubscribeToInteractionEvent(VRTK_InteractableObject.InteractionType.Use,StartShower);
            shower = transform.GetChild(0).gameObject;
        }

        public void StartShower(object sender, InteractableObjectEventArgs args)
        {
            shower.SetActive(true);
        }
    }
}
