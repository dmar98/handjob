﻿using UnityEngine;

namespace Handjob.Scripts.Extra
{
    [RequireComponent(typeof(Collider))]
    public class RandomColorChooser : MonoBehaviour
    {
        private Material _primaryMaterial, _secondaryMaterial;
        private Color _primaryC, _secondaryC;

        // Start is called before the first frame update
        private void Start()
        {
            InstantiateMaterials();
        
            float r1 = Random.Range(0.0f, 1.0f),
                g1 = Random.Range(0.0f, 1.0f),
                b1 = Random.Range(0.0f, 1.0f),
                r2 = Random.Range(0.0f, 1.0f),
                g2 = Random.Range(0.0f, 1.0f),
                b2 = Random.Range(0.0f, 1.0f);
        
            ReplaceColors(r1,g1,b1,r2,g2,b2);
        }

        private void UpdateMaterials()
        {
            _primaryMaterial.color = _primaryC;
            _secondaryMaterial.color = _secondaryC;
        }

        private void InstantiateMaterials()
        {
            //normal shader
            _primaryMaterial = new Material(Shader.Find("Custom/CustomMaterialShader"));
            _secondaryMaterial = new Material(Shader.Find("Custom/CustomMaterialShader"));

            _primaryMaterial.name = "Primary";
            _secondaryMaterial.name = "Secondary";

            UpdateMaterials();
        
            MeshRenderer[] array = GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer meshRenderer in array)
            {
                switch (meshRenderer.material.name)
                {
                    //Debug.Log("Found");
                    //Debug.Log(renderer.material.name.ToString());
                    case "Primary (Instance)":
                        //Debug.Log("Primary");
                        meshRenderer.material = _primaryMaterial;
                        break;
                    case "Secondary (Instance)":
                        //Debug.Log("Secondary");
                        meshRenderer.material = _secondaryMaterial;
                        break;
                }
            }
        }
        //main public function -------CALL THIS-------
        private void ReplaceColors(float r1, float g1, float b1, float r2, float g2, float b2)
        {
            _primaryC = new Color(r1, g1, b1);
            _secondaryC = new Color(r2, g2, b2);
       
            UpdateMaterials();
        }
    
        //when touched by a hand this replaces it's colors by this object's colors (primary / secondary)
        public void OnCollisionEnter(Collision other)
        {
            Debug.Log("Collision");
            //if other object is in hand layer
            if (other.collider.gameObject.layer == 11)
            {
                Debug.Log("Found");
                other.gameObject.GetComponent<ColorChanger>().ReplaceColors(_primaryC, _secondaryC);
            }
        }
    }
}
