﻿using UnityEngine;

namespace Handjob.Scripts.Extra
{
    [RequireComponent(typeof(Collider))]
    public class ColorChooser : MonoBehaviour
    {
        private Color _primaryC, _secondaryC;
    
        // Start is called before the first frame update
        private void Start()
        {
            InstantiateColors();
        }
    
        //when touched by a hand this replaces it's colors by this object's colors (primary / secondary)
        public void OnCollisionEnter(Collision other)
        {
            //Debug.Log("Collision");
            //if other object is in hand layer
            if (other.collider.gameObject.layer == 11)
            {
                //Debug.Log("Found");
                other.gameObject.GetComponent<ColorChanger>().ReplaceColors(_primaryC, _secondaryC);
            }
        }
    
        //look what the primary and secondary colors are and save them
        private void InstantiateColors()
        {
            MeshRenderer[] array = GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer meshRenderer in array)
            {
                switch (meshRenderer.material.name)
                {
                    case "Primary (Instance)":
                        _primaryC = meshRenderer.material.color;
                        break;
                    case "Secondary (Instance)":
                        _secondaryC = meshRenderer.material.color;
                        break;
                }
            }
        }
    }
}
