﻿using UnityEngine;

namespace Handjob.Scripts.Door
{
    public class KeyCard : MonoBehaviour
    {
        [SerializeField] private int keyCode;

        public int GetKeyCode()
        {
            return keyCode;
        }
    }
}
