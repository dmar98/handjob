﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Handjob.Scripts.Conveyor
{
    public class ConveyorMaster : MonoBehaviour
    {

        [SerializeField][Range(1,10)] private int speed;
    
        // Start is called before the first frame update
        private void Start()
        {
            StartCoroutine(StartConveyors());
        }

        private IEnumerator StartConveyors()
        {
            List<LinearConveyor> conveyors = new List<LinearConveyor>(GetComponentsInChildren<LinearConveyor>());
            foreach (LinearConveyor linearConveyor in conveyors)
            {
                linearConveyor.speed *= speed;
            }
        
            /*List<RadialConveyor> radConveyors;
        radConveyors = new List<RadialConveyor>(GetComponentsInChildren<RadialConveyor>());
        foreach (RadialConveyor radConveyor in radConveyors)
        {
            radConveyor.speed *= speed;
        }
*/
            yield return null;
        }
    }
}
