﻿using UnityEngine;

namespace Handjob.Scripts.Conveyor
{
    public class DestroyOnCollision : MonoBehaviour
    {
        private void OnCollisionEnter(Collision other)
        {
            Destroyable dest;
            if ((dest = other.gameObject.GetComponent<Destroyable>()) != null && dest.DestroyEnabled)
            {
                dest.Destroy();
            }
        }
    }
}
