﻿using VRTK;

namespace Handjob.Scripts.Player
{
    public class Stop : HandState
    {
        public Stop(AbstractHand hand) : base(hand)
        {
            _hand.GetRigidbody().useGravity = true;
            _hand.GetCollider().enabled = true;
        }

        public override void PerformAction(object sender, ControllerInteractionEventArgs e)
        {
            _hand.Stop();
        }
    }
}