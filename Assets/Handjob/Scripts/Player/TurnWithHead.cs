﻿using UnityEngine;

namespace Handjob.Scripts.Player
{
    public class TurnWithHead : MonoBehaviour
    {
        [SerializeField] private GameObject gameObjectToFollow;

        private void Start()
        {
            gameObjectToFollow = GameObject.Find("body_JNT");
        }

        private void Update()
        {
            if (gameObjectToFollow == null)
            {
                gameObjectToFollow = GameObject.Find("body_JNT");
            }
            else
            {
                transform.rotation =
                    Quaternion.Euler(new Vector3(0, gameObjectToFollow.transform.localRotation.eulerAngles.y, 0));
            }
        }
    }
}