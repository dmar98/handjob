﻿using System.Collections.Generic;
using UnityEngine;

namespace Handjob.Scripts.Player
{
    public class Wrist : MonoBehaviour
    {
        private GameObject _screen;
        private MeshRenderer _meshRenderer;
        [SerializeField] private List<Material> healthMaterialList = new List<Material>();
    
        private void Start()
        {
            _screen = transform.Find("screen").gameObject;
            _meshRenderer = _screen.GetComponent<MeshRenderer>();

            SetHealthUI(5);
        }

        public void SetHealthUI(int health)
        {
            if (health > -1 && health < 6)
            {
                _meshRenderer.material = healthMaterialList[health - 1];
            }
        }
    }
}
