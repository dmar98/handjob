﻿using System.Collections.Generic;
using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    [RequireComponent(typeof(Collider)), RequireComponent(typeof(Rigidbody))]
    public class Attachable : MonoBehaviour
    {
        private AbstractHand _abstractHand;
        private Collider _collider;
        private Rigidbody _rigidbody;
        private VRTK_AvatarHandController _vrtkAvatarHandController;
        private HandPosition _handPosition;

        /// <summary>
        /// Get all required Components
        /// </summary>
        private void Awake()
        {
            _abstractHand = GetComponentInChildren<AbstractHand>();
            _collider = GetComponent<Collider>();
            _rigidbody = GetComponent<Rigidbody>();
            _vrtkAvatarHandController = GetComponentInChildren<VRTK_AvatarHandController>();
            _handPosition = GetComponent<HandPosition>();
        }

        /// <summary>
        /// Attaches a hand to a players hand
        /// </summary>
        /// <param name="attache">The players hand(right or left)</param>
        public void Attach(Attache attache)
        {
            VRTK_ControllerEvents events = attache.GetComponent<VRTK_ControllerEvents>();
            transform.SetParent(attache.transform);
            _abstractHand.Attached(events);
            _vrtkAvatarHandController.enabled = true;
            _vrtkAvatarHandController.controllerEvents = events;
            _collider.enabled = false;
            _rigidbody.useGravity = false;
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = Vector3.zero;

            bool isRightHand = attache.IsRight();
            _handPosition.SetPosition(isRightHand);
        }

        /// <summary>
        /// Detaches the attached hand
        /// </summary>
        public bool Detach(IEnumerable<Belt> belt, GameObject hand)
        {
            if (_abstractHand.GetState() is Shot)
            {
                bool available = false;
                foreach (Belt beltSlot in belt)
                {
                    if (beltSlot.Available(hand))
                    {
                        available = true;
                        transform.SetParent(beltSlot.transform);
                        _abstractHand.DetachOnToBelt();
                        break;
                    }
                }

                if (!available)
                {
                    transform.SetParent(null);
                    _abstractHand.Detached();
                    _rigidbody.constraints = RigidbodyConstraints.None;
                    _rigidbody.useGravity = true;
                }

                _collider.enabled = true;
                _vrtkAvatarHandController.enabled = false;
                return true;
            }

            return false;
        }
    }
}