﻿using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    public class Attache : MonoBehaviour
    {
        private VRTK_ControllerEvents vrtkControllerEvents;
        private Attachable currentHand;

        [SerializeField] private LayerMask mask;
        [SerializeField] private GameObject belt;
        [SerializeField] private bool isRightHand;

        private Belt[] beltSlots = new Belt[2];

        /// <summary>
        /// gets components 
        /// </summary>
        private void Awake()
        {
            vrtkControllerEvents = GetComponent<VRTK_ControllerEvents>();
        }

        /// <summary>
        /// initializes belt slots and listens for input
        /// </summary>
        private void Start()
        {
            int i = 0;
            vrtkControllerEvents.ButtonTwoPressed += Attach;
            foreach (Transform beltSlot in belt.transform)
            {
                beltSlots[i++] = beltSlot.gameObject.GetComponent<Belt>();
            }
        }

        /// <summary>
        /// casts a sphere and the first hit hand is attached 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="controllerInteractionEventArgs"></param>
        private void Attach(object sender, ControllerInteractionEventArgs controllerInteractionEventArgs)
        {
            Collider[] hits = new Collider[5];
            Physics.OverlapSphereNonAlloc(transform.position, 0.25f, hits, mask);

            if (hits[0] != null && currentHand == null)
            {
                currentHand = hits[0].GetComponent<Attachable>();
                currentHand.Attach(this);
            }
            else if (currentHand != null)
            {
                DetachHandChild();
            }
        }

        /// <summary>
        /// detaches the hand
        /// </summary>
        private void DetachHandChild()
        {
            if (currentHand != null)
            {
                if (currentHand.Detach(beltSlots, gameObject))
                {
                    currentHand = null;
                }
            }
        }

        public bool IsRight()
        {
            return isRightHand;
        }
    }
}