﻿using UnityEngine;

namespace Handjob.Scripts.Player
{
    public class Belt : MonoBehaviour
    {
        private bool isIn;
        private GameObject hand;

        private MeshRenderer _meshRenderer;
        [SerializeField] private Material[] _material = new Material[2];

        private void Awake()
        {
            _meshRenderer = GetComponentInChildren<MeshRenderer>();
        }


        /// <summary>
        /// controls if it is possible to add something to the belt
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Controller"))
            {
                hand = other.gameObject;
                _meshRenderer.material = _material[1];
            }

            isIn = transform.childCount == 1;
        }

        /// <summary>
        /// not in range to add it to belt slot
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerExit(Collider other)
        {
            hand = null;
            isIn = false;
            _meshRenderer.material = _material[0];
        }

        /// <summary>
        /// returns if belt slot is available to be filled
        /// </summary>
        /// <returns></returns>
        public bool Available(GameObject hand)
        {
            return isIn && hand == this.hand;
        }
    }
}