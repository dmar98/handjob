﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Handjob.Scripts.Player
{
    public class Player : MonoBehaviour
    {
        private int health;
        [SerializeField] private Wrist[] _wrists;

        private void Start()
        {
            health = 5;
        }

        public void Damage()
        {
            health -= 1;
            foreach (Wrist wrist in _wrists)
            {
                wrist.SetHealthUI(health);
            }
            if (health == 0)
            {
                Die();
            }
        }

        private void Die()
        {
            SceneManager.LoadScene(0);
        }
    }
}
