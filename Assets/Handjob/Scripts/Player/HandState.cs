﻿using VRTK;

namespace Handjob.Scripts.Player
{
    public abstract class HandState
    {
        protected AbstractHand _hand;

        protected HandState(AbstractHand hand)
        {
            _hand = hand;
        }

        public abstract void PerformAction(object sender, ControllerInteractionEventArgs e);
    }
}