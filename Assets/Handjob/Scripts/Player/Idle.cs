﻿using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    public class Idle : HandState
    {
        /// <summary>
        /// hand is frozen while parent is affected by gravity
        /// </summary>
        /// <param name="hand"></param>
        public Idle(AbstractHand hand) : base(hand)
        {
            _hand.GetRigidbody().constraints = RigidbodyConstraints.FreezeAll;
            _hand.GetRigidbody().useGravity = true;
            _hand.GetCollider().enabled = false;
            _hand.GetParent().GetComponent<Rigidbody>().useGravity = true;
            _hand.GetParent().GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            _hand.GetParent().GetComponent<Rigidbody>().velocity = Vector3.zero;
            _hand.GetParent().GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }

        public override void PerformAction(object sender, ControllerInteractionEventArgs e)
        {
            _hand.Idle();
        }
    }
}