﻿using Handjob.Scripts.AI;
using Handjob.Scripts.Game;
using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    public class HandFingerGun : AbstractHand
    {
        private bool _isMoving;
        private bool _isShot;

        #region Unity Methodes

        /// <summary>
        /// sets beginning state
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            parent = transform.parent.parent;
            ChangeState(new Idle(this));
        }

        /// <summary>
        /// stops the hand after a certain distance and if it is called back it gets put on to the players hand
        /// </summary>
        private void Update()
        {
            if (IsParentless() && _isMoving)
            {
                if (_isShot)
                {
                    if (_state is Flying)
                    {
                        if (Vector3.Distance(parent.position, transform.position) > 20f)
                        {
                            _isMoving = false;
                            _isShot = false;
                            StoppedHand();
                            ChangeState(new Recall(this));
                        }
                    }
                }
                else
                {
                    if (_state is Recall)
                    {
                        if (Vector3.Distance(parent.position, transform.position) < 0.1f &&
                            !(_state is Idle))
                        {
                            StoppedHand();
                            transform.SetParent(parent.GetChild(1));

                            ResetPositionToZero();
                            _isMoving = false;
                            _isShot = false;
                            _collider.enabled = false;
                            ChangeState(new Shot(this));
                        }
                    }
                }
            }
        }

        #endregion

        /// <summary>
        /// checks if the root of the game object is the hand
        /// </summary>
        /// <returns></returns>
        private bool IsParentless()
        {
            return transform.root == transform;
        }

        /// <summary>
        /// on collision stop the hand
        /// </summary>
        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.layer == 12)
            {
                MySimpleAI ai = other.transform.GetComponentInParent<MySimpleAI>();
                if (ai != null)
                {
                    ai.Die();
                }
                
            }
            _isMoving = false;
            _isShot = false;
            StoppedHand();
            _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.ShootHit, gameObject);
            ChangeState(new Recall(this));
        }
        
        

        /// <summary>
        /// resets its position 
        /// </summary>
        public override void ResetPosition()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.Euler(new Vector3(90, 0, 0));
        }

        /// <summary>
        /// hand is attached so state change
        /// </summary>
        /// <param name="getComponent"></param>
        public override void Attached(VRTK_ControllerEvents getComponent)
        {
            base.Attached(getComponent);
            _isMoving = false;
            _isShot = false;
            ChangeState(new Shot(this));
        }

        /// <summary>
        /// hand is detached and state is changed 
        /// </summary>
        public override void Detached()
        {
            base.Detached();
            ResetPosition();
            ChangeState(new Idle(this));
        }

        /// <summary>
        /// makes it so hand is on the belt slot after detachment
        /// </summary>
        public override void DetachOnToBelt()
        {
            base.DetachOnToBelt();
            ResetPositionToZero();
            ChangeState(new OnBelt(this));
        }

        /// <summary>
        /// position zero is not quite zero on local rotation
        /// </summary>
        protected override void ResetPositionToZero()
        {
            base.ResetPositionToZero();
            transform.localRotation = Quaternion.Euler(90, 0, 0);
        }

        /// <summary>
        /// shots the finger in the players hand forward direction
        /// </summary>
        public override void Shot()
        {
            if (!_isMoving)
            {
                _rigidbody.constraints = RigidbodyConstraints.None;
                transform.SetParent(null);
                _rigidbody.AddForce(parent.transform.forward.normalized * force);
                _isMoving = true;
                _isShot = true;
                _collider.enabled = true;
                _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.Shoot, gameObject);
                ChangeState(new Flying(this));
            }
        }

        /// <summary>
        /// calls the finger back to the player
        /// </summary>
        public override void Recall()
        {
            if (!_isMoving)
            {
                _rigidbody.useGravity = false;
                _rigidbody.constraints = RigidbodyConstraints.None;
                Vector3 dir = parent.position - transform.position;
                _rigidbody.AddForce(dir.normalized * force);
                _isMoving = true;
            }
        }

        public override void Stop()
        {
        }
    }
}