﻿using Handjob.Scripts.Game;
using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    public class DuckHand : AbstractHand
    {
        /// <summary>
        /// Sets the State
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            ChangeState(new Idle(this));
        }

        #region Possible Actions

        /// <summary>
        /// On shot a sound is played
        /// </summary>
        public override void Shot()
        {
            _soundHandlerActionEvent?.Invoke(SoundEventHandler.PlayerActionEventType.Duck, gameObject);
        }

        public override void Recall()
        {
        }


        /// <summary>
        /// Hand is not usable while sound is played
        /// </summary>
        public override void Stop()
        {
        }

        #endregion

        /// <summary>
        /// Hand was attached
        /// </summary>
        /// <param name="getComponent"></param>
        public override void Attached(VRTK_ControllerEvents getComponent)
        {
            base.Attached(getComponent);
            ChangeState(new Shot(this));
        }

        /// <summary>
        /// Hand was detached
        /// </summary>
        public override void Detached()
        {
            base.Detached();
            ChangeState(new Idle(this));
        }

        /// <summary>
        /// Resets hand to its default rotation and transform
        /// </summary>
        public override void ResetPosition()
        {
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }
    }
}