﻿using UnityEngine;
using VRTK;

namespace Handjob.Scripts.Player
{
    public class Shot : HandState
    {
        public Shot(AbstractHand hand) : base(hand)
        {
            _hand.GetRigidbody().constraints = RigidbodyConstraints.FreezeAll;
            _hand.GetRigidbody().useGravity = false;
        }

        public override void PerformAction(object sender, ControllerInteractionEventArgs e)
        {
            if (_hand.GetParent() != null)
            {
                _hand.Shot();
            }
        }
    }
}