using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class AIPatrollingState : AIState
    {
        private GameObject lastKnownPosition;
        private float maxPatrolTime;
        private float patrolTime;
        private Vector3 initialPosition;

        public AIPatrollingState(MySimpleAI parent, GameObject g, float maxPatrolTime = 10) : base(parent)
        {
            lastKnownPosition = g;
            this.maxPatrolTime = maxPatrolTime;
            patrolTime = maxPatrolTime;
            defaultAnimation = 1;
            specialAnimation = 2;
        }

        public override bool Update(Perception.SeeResult seeResult)
        {
            bool stateChanged = false;
            if (!isActive) Activate();
            if (seeResult == Perception.SeeResult.SEES_PLAYER) stateChanged = ChangeState(ai.GetAIFollowingState());
            else
            {
                if (animEnded)
                {
                    if (patrolTime > 0)
                    {
                        if (ai.PointReached(false))
                        {
                            NewRandomDestination();
                        }
                    }
                    else
                    {
                        if (ai.PointReached(false)) stateChanged = ChangeState(ai.GetAIPathState());
                    }
                    patrolTime -= Time.deltaTime;
                }
            }
            return stateChanged;
        }

        public override Vector3 GetNextPoint()
        {
            return destination;
        }

        private void StartAnim()
        {
            animEnded = false;
            ai.ChangeAnimationState(2);
        }

        protected override void Activate()
        {
            StartAnim();
            base.Activate();
        }

        protected override void DeActivate()
        {
            animEnded = false;
            patrolTime = maxPatrolTime;
            base.DeActivate();
        }

        private void NewRandomDestination()
        {
            destination = new Vector3(lastKnownPosition.transform.position.x + Random.Range(1, 5),
                lastKnownPosition.transform.position.y, lastKnownPosition.transform.position.z + Random.Range(1, 5));
        }
    }
}