using UnityEngine;

namespace Handjob.Scripts.AI
{
    /// <summary>
    /// This is the following state of the AI, as long as you're in visible range the AI will follow, and also follow your last known position when you're out of sight.
    /// </summary>
    public class AIFollowingState : AIState
    {
        /// <summary>
        /// We keep a reference to the target, the position in memory of you that the AI keeps.
        /// </summary>
        private GameObject target;

        public AIFollowingState(MySimpleAI parent, GameObject objectToFollow) : base(parent)
        {
            target = objectToFollow;
        }

        public override bool Update(Perception.SeeResult seeResult)
        {
            bool stateChanged = false;
            if (!isActive) Activate();
            if(seeResult == Perception.SeeResult.IN_ATTACK_RANGE) stateChanged = ChangeState(ai.GetAIAttackingState());
            if (seeResult != Perception.SeeResult.SEES_PLAYER)
            {
                if (ai.PointReached(true)) stateChanged = ChangeState(ai.GetAIPatrollingState());
                else destination = target.transform.position;
            }
            destination = target.transform.position;
            return stateChanged;
        }

        public override Vector3 GetNextPoint()
        {
            return destination;
        }

        protected override void Activate()
        {
            ai.ChangeAnimationState(1); 
            base.Activate();
        }
        
    }
}