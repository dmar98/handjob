﻿using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class Throw : MonoBehaviour
    {
        private Rigidbody _rb;
        private Transform parentTransform;
        private Collider _collider;

        public Vector3 targetPosition;
    
        private void Awake()
        {
            enabled = false;
        
            _rb = GetComponent<Rigidbody>();
            _rb.useGravity = false;
            setRBActive(false);
        
            _collider = GetComponent<Collider>();
            _collider.enabled = false;
        }

        private void setRBActive(bool enable)
        {
            _rb.constraints = enable ? RigidbodyConstraints.None : RigidbodyConstraints.FreezeAll;
        }

        public void Release()
        {
            parentTransform = transform.parent;
            transform.parent = null;
            enabled = true;
            _collider.enabled = true;
            _rb.useGravity = true;
            setRBActive(true);
            _rb.AddForce((targetPosition - transform.position).normalized * 8, ForceMode.Impulse);
        }
    
        /* private void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, targetPosition)< minDist)
        {
            Hit();
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.fixedDeltaTime);
        }
    }*/

        private void OnCollisionEnter()
        {
            Hit();
        }

        private void Hit()
        {
            //Hit
            enabled = false;
            setRBActive(false);
            transform.SetParent(parentTransform);
            transform.position = transform.parent.position;
            _collider.enabled = false;
            gameObject.SetActive(false);
        }
    }
}
