﻿using UnityEngine;

namespace Handjob.Scripts.AI
{
    public class AIMovement : MonoBehaviour, IMovement
    {

        public float moveSpeed;
    
        public void Follow(Vector3 target)
        {
            MoveTo(target);
            RotateTo(target);
        }
        public void MoveTo(Vector3 targetPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * moveSpeed);
        }

        public void RotateTo(Vector3 targetPosition)
        {
            Vector3 lookAtVector = Vector3.RotateTowards(transform.forward,  
                (targetPosition - transform.position).normalized ,Time.deltaTime, 0f);
            transform.LookAt(transform.position + lookAtVector);
        }

        public void RotateTo(Quaternion quaternion)
        {
        
        }
    }
}
