﻿namespace Handjob.Scripts.Interfaces
{
    public interface IHandActions
    {
        void Shot();
        void Recall();
        void Idle();
        void Stop();
        void OnBelt();
        void Fly();
    }
}