﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Handjob.Scripts.InGameMenu
{
    public class MenuFunctions : MonoBehaviour
    {
        public void ExitTToMainMenu()
        {
            SceneManager.LoadScene(0);
        }
    }
}
