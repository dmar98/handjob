﻿using System;
using UnityEngine;

namespace Handjob.Scripts.Game
{
    public class GameEvents : MonoBehaviour
    {
        public static GameEvents instance;

        private void Awake()
        {
            instance = this;
        }

        public event Action<int> onDoorWayTriggerEnter;

        public void DoorWayTriggerEnter(int id)
        {
            onDoorWayTriggerEnter?.Invoke(id);
        }
    }
}
