﻿using System.Collections.Generic;
using UnityEngine;

namespace Handjob.Scripts.Game
{
    public class Room : MonoBehaviour
    {
        public bool startDisabled;

        private bool roomsEnabled = true;

        [SerializeField] private List<GameObject> disablableThings;

        private BoxCollider coll;

        private void Start()
        {
            coll = GetComponent<BoxCollider>();
            roomsEnabled = !startDisabled;
            //Does not work due to initialization, just disable manually 
            if (startDisabled)
            {
                Disable();
            }
        }

        public void Enable()
        {
            if (!roomsEnabled)
            {
                roomsEnabled = true;

                EnableRigidbodies(true);

                foreach (GameObject disablableThing in disablableThings)
                {
                    disablableThing.SetActive(true);
                }
            }
        }

        public void Disable()
        {
            if (roomsEnabled)
            {
                roomsEnabled = false;

                EnableRigidbodies(false);

                foreach (GameObject disablableThing in disablableThings)
                {
                    disablableThing.SetActive(false);
                }
            }
        }

        public void EnableRigidbodies(bool enable)
        {
            Collider[] colliders = Physics.OverlapBox(transform.position + coll.center, coll.size);

            foreach (Collider item in colliders)
            {
                Rigidbody rb;
                if ((rb = item.GetComponent<Rigidbody>()) != null) rb.useGravity = enable;
            }
        }
    }
}