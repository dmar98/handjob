﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Handjob.Scripts.Game
{
    public class Exit : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            SceneManager.LoadScene(0);
        }
    }
}
