﻿using UnityEngine;

namespace Handjob.Scripts.Menu
{
    public class ControlsButton : UIItem
    {

        [SerializeField] private GameObject controls;

        public override void Hover()
        {
            _tmp.color = hoverColor;
            controls.SetActive(true);
        }

        public override void StopHover()
        {
            _tmp.color = inactiveColor;
            controls.SetActive(false);
        }

        public override void Action()
        {
            throw new System.NotImplementedException();
        }
    }
}

