﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace Handjob.Scripts.Menu
{
    public class Menu : MonoBehaviour
    {
        [FMODUnity.EventRef] public string countdownClick;
    
        [SerializeField] private Color inactiveColor = Color.white;
        [SerializeField] private Color hoverColor = Color.cyan;
        [SerializeField] private Color screenIdleColor = Color.black;
        [SerializeField] private Color screenActiveColor = Color.yellow;

        private TextMeshPro _tmp;
    
        private Material screenMaterial;
        private UIItem currentlyActiveUiItem;
        private bool countdown;
        private GameObject child;

        private void Awake()
        {
            screenMaterial = transform.parent.gameObject.GetComponent<MeshRenderer>().materials[0];
            _tmp = GetComponent<TextMeshPro>();
            child = transform.GetChild(0).gameObject;
        }
    
        void Start()
        {
            ShowCredits(false);
            StopHover();
        }

        private void Hover()
        {
            _tmp.color = hoverColor;
            screenMaterial.color = screenActiveColor;
            countdown = true;
            StartCoroutine(StartSequence());
        }
    
        public void MenuHover(UIItem item)
        {
            Hover();
            Register(item);
        }

        private void Register(UIItem item)
        {
            currentlyActiveUiItem = item;
        }

        public void StopHover()
        {
            StopAllCoroutines();
            if(_tmp != null) _tmp.color = inactiveColor;
            screenMaterial.color = screenIdleColor;
            countdown = false;
        }

        private IEnumerator StartSequence()
        {
            _tmp.SetText("0");
            yield return new WaitForSeconds(1);
            if (countdown)
            {
                _tmp.SetText("1");
                FMODUnity.RuntimeManager.PlayOneShot(countdownClick);
                yield return new WaitForSeconds(1);
                if (countdown)
                {
                    _tmp.SetText("2");
                    FMODUnity.RuntimeManager.PlayOneShot(countdownClick);
                    yield return new WaitForSeconds(1);
                    if (countdown)
                    {
                        _tmp.SetText("3");
                        currentlyActiveUiItem.ActivateButton();
                    }
                }
            }
        }

        public void ShowCredits(bool show)
        {
            child.SetActive(show);
        }
    
    }
}
