﻿Shader "Custom/Outline"
{
    Properties{
        _MainTex("Main Texture (RGB)", 2D) = "white" {}
        _Color("Color",Color) = (1,1,1,1)
        
        _OutlineTex("Outline Texture", 2D) = "white" {}
        _OutlineColor("Outline Color", Color) = (1,1,1,1)
        _OutlineWidth("Outline Width", Range(1,10)) = 1.1
    }
    
    SubShader
    {
    
        //We can have any number of passes
        //Each pass is one "Render Pass", means rendering it once
    
        Tags
        {
            "Queue" = "Transparent"
        }
    
        //Pass for the Outline
        Pass{
            //Name for if we want to use it in another function
            Name "OUTLINE"
            
            //WE NEED TO DISABLE Z WRITING SO THAT WE DO NOT DRAW THE OUTLINE ON TOP -> WE ONLY SEE THE OUTLINE COLOR THEN
            ZWrite Off
            
            CGPROGRAM //Allows talk between two languages: shader lab and nvidia C for graphics.
                
                //Every program needs a vertex function and a fragment function
                
                //define for building function (shape)
                #pragma vertex vert
                //define for coloring function
                #pragma fragment frag
                
                //Includes
                //Built-in Shader Functions by Unity
                #include "UnityCG.cginc"
                
                //Structures on how things get data
                //First appdata -> defines how vertex function gets its info
                struct appdata{
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };
                
                //v2f is how the fragment function gets its data
                //use SV_POSITION instead of POSITION so it works on every platform
                struct v2f{
                    float4 pos: SV_POSITION;
                    float2 uv : TEXCOORD0;
                };
                
                
                /*
                    IMPORTS
                    
                    We need to re-import the things that we defined at the very top at the Properties which are publicly exposed.
                    We do this because we just basically switched languages as soon as CGPROGRAM started.
                    Here we need to redefine these imports to get them working in this scope.
                */
                
                //They have to have the same name as above
                
                sampler2D _OutlineTex;
                float4 _OutlineColor;
                float _OutlineWidth;
                
                //Vertex Function
                // v2f is what it returns, the input for frag, vert is the name of the function we defined earlier and appdata is the struct we also defined earlier
                v2f vert(appdata IN){
                    //We scale the entire object by the OutlineWidth Factor
                    IN.vertex.xyz *= _OutlineWidth;
                    v2f OUT;
                    
                    //Takes the object from the object space and puts it into camera clip space
                    OUT.pos = UnityObjectToClipPos(IN.vertex);
                    OUT.uv = IN.uv;
                    
                    return OUT;
                }
                
                //Fragment function
                //SV_Target : System-Value Target, just specifying that it is a Rendertarget, more info : https://cgcookie.com/questions/8160-after-watching-the-video-there-were-a-few-points-i-wanted-to-clarify-if-that-was-ok
                fixed4 frag(v2f IN) : SV_Target
                {
                    float4 texColor = tex2D(_OutlineTex,IN.uv);
                    return texColor * _OutlineColor;
                }
                
            ENDCG
        }    
        
       //Additional Pass for the Normal Shape
        Pass{
            Name "OBJECT"
            CGPROGRAM //Allows talk between two languages: shader lab and nvidia C for graphics.
                
                //Every program needs a vertex function and a fragment function
                
                //define for building function (shape)
                #pragma vertex vert
                //define for coloring function
                #pragma fragment frag
                
                //Includes
                //Built-in Shader Functions by Unity
                #include "UnityCG.cginc"
                
                //Structures on how things get data
                //First appdata -> defines how vertex function gets its info
                struct appdata{
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };
                
                //v2f is how the fragment function gets its data
                //use SV_POSITION instead of POSITION so it works on every platform
                struct v2f{
                    float4 pos: SV_POSITION;
                    float2 uv : TEXCOORD0;
                };
                
                
                /*
                    IMPORTS
                    
                    We need to re-import the things that we defined at the very top at the Properties which are publicly exposed.
                    We do this because we just basically switched languages as soon as CGPROGRAM started.
                    Here we need to redefine these imports to get them working in this scope.
                */
                
                //They have to have the same name as above
                
                float4 _Color;
                sampler2D _MainTex;
                
                //Vertex Function
                // v2f is what it returns, the input for frag, vert is the name of the function we defined earlier and appdata is the struct we also defined earlier
                v2f vert(appdata IN){
                    //Initialize what we return
                    v2f OUT;
                    
                    //Takes the object from the object space and puts it into camera clip space
                    OUT.pos = UnityObjectToClipPos(IN.vertex);
                    OUT.uv = IN.uv;
                    
                    return OUT;
                }
                
                //Fragment function
                //SV_Target : System-Value Target, just specifying that it is a Rendertarget, more info : https://cgcookie.com/questions/8160-after-watching-the-video-there-were-a-few-points-i-wanted-to-clarify-if-that-was-ok
                fixed4 frag(v2f IN) : SV_Target
                {
                    float4 texColor = tex2D(_MainTex,IN.uv);
                    return texColor * _Color;
                }
                
            ENDCG
        }    
    }
    
}
