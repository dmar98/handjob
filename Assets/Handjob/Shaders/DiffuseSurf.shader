﻿Shader "CelShading/DiffuseSurf"
{
    Properties
    {
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Color ("Tint Color", Color) = (1,1,1,1)
        _Antialiasing("Band Smoothing", Float) = 5.0
        _Glossiness("Glossiness/Shininess", Float) = 400
        _NormalMap("Normal/Bump Map", 2D) = "bump" {}
        _Fresnel("Fresnel/Rim Amount", Range(0, 1)) = 0.5
        
        _OutlineSize("Outline Size", Float) = 0.01
        _OutlineColor("Outline Color", Color) = (0, 0, 0, 1)
    }

    SubShader
    {
        Tags 
        {
         "RenderType"="Opaque" 
        }
        
        Stencil
        {
            Ref 1
            Comp always
            Pass replace
            Fail keep
            ZFail keep
        }
        
        //Declare which lightning model to use -> it's Cel which is the one we created ourselves
        CGPROGRAM
        #pragma surface surf Cel

        sampler2D _MainTex;
        sampler2D _NormalMap;
        fixed4 _Color;
        float _Antialiasing;
        float _Glossiness;
        float _Fresnel;

        // Lightning model we use, it's custom to create the "steps" known in Cel shading
        float4 LightingCel(SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
        {
            //We calculate the diffuse lightning value by getting the dot between the current vertex and the lightdir
            float3 normal = normalize(s.Normal);
            float diffuse = dot(normal, lightDir);

            //By tracking the changes in the diffuse value with fwidth, we get a delta value that we pipe into an inverse lerp to know how far we are in the lightning transition
            float delta = fwidth(diffuse) * _Antialiasing;
            float diffuseSmooth = smoothstep(0, delta, diffuse);
            
            //Two-Step lightning
            //float diffuseTwoStep = diffuse > 0 ? 1 : 0;
            
            //We get a half vector, pointing halfway from the players forward and halfway from the lightsource
            //We use that vector to calculate the specular with the dot product yet again
            float3 halfVec = normalize(lightDir + viewDir);
            float specular = dot(normal, halfVec);
            
            //We use our glossiness value for better results but we also factor in the diffuse because there's no specular light in a shaded area
            specular = pow(specular * diffuseSmooth, _Glossiness);
            
            //We also smoothstep the specular but we do it smoothly (heh) as the result doesnt look good with actual steps. We factor in our own parameter value for the steps.
            float specularSmooth = smoothstep(0, 0.01 * _Antialiasing, specular);
            
            //Rim lightning needs the invert of the dot product, the more perpendicular the better!
            float rim = 1 - dot(normal, viewDir);
            
            // Don't apply rim lighting in shaded areas.
            rim = rim * diffuse;
            
            float fresnelSize = 1 - _Fresnel;
            
            float rimSmooth = smoothstep(fresnelSize, fresnelSize * 1.1, rim);
            
            float3 col = s.Albedo * ((diffuseSmooth + specularSmooth + rimSmooth) * _LightColor0 + unity_AmbientSky);
            return float4(col, s.Alpha);
        }

        struct Input
        {
            float2 uv_MainTex;
            float2 uv_Normal;
        };

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_Normal));
        }

        ENDCG
        
        
        Stencil
        {
            Ref 1
            Comp notequal
        }
        
        Pass
        {
            
            ZWrite off
            ZTest on
        
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
        
            #include "UnityCG.cginc"
        
            float _OutlineSize;
            float4 _OutlineColor;
            
            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            
            struct v2f 
            {
                float4 vertex : SV_POSITION;
            };
        
        v2f vert(appdata v)
        {
            v2f o;
            float3 normal = normalize(v.normal) * _OutlineSize;
            float3 pos = v.vertex + normal;
        
            o.vertex = UnityObjectToClipPos(pos);
            return o;
        }

        float4 frag(v2f i) : SV_Target
        {
            return _OutlineColor;
        }
        
        
            ENDCG
        }
        
    }

    FallBack "Diffuse"
}