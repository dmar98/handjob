﻿Shader "Custom/BasicUnlit"
{
    Properties{
        _MainTex("Main Texture (RGB)", 2D) = "white" {}
        _Color("Color",Color) = (1,1,1,1)
    }
    
    SubShader
    {
    
        //We can have any number of passes
        //Each pass is one "Render Pass", means rendering it once
    
        Pass{
            CGPROGRAM //Allows talk between two languages: shader lab and nvidia C for graphics.
                
                //Every program needs a vertex function and a fragment function
                
                //define for building function (shape)
                #pragma vertex vert
                //define for coloring function
                #pragma fragment frag
                
                //Includes
                //Built-in Shader Functions by Unity
                #include "UnityCG.cginc"
                
                //Structures on how things get data
                //First appdata -> defines how vertex function gets its info
                struct appdata{
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                };
                
                //v2f is how the fragment function gets its data
                //use SV_POSITION instead of POSITION so it works on every platform
                struct v2f{
                    float4 pos: SV_POSITION;
                    float2 uv : TEXCOORD0;
                };
                
                
                /*
                    IMPORTS
                    
                    We need to re-import the things that we defined at the very top at the Properties which are publicly exposed.
                    We do this because we just basically switched languages as soon as CGPROGRAM started.
                    Here we need to redefine these imports to get them working in this scope.
                */
                
                //They have to have the same name as above
                
                float4 _Color;
                sampler2D _MainTex;
                
                //Vertex Function
                // v2f is what it returns, the input for frag, vert is the name of the function we defined earlier and appdata is the struct we also defined earlier
                v2f vert(appdata IN){
                    //Initialize what we return
                    v2f OUT;
                    
                    //Takes the object from the object space and puts it into camera clip space
                    OUT.pos = UnityObjectToClipPos(IN.vertex);
                    OUT.uv = IN.uv;
                    
                    return OUT;
                }
                
                //Fragment function
                //SV_Target : System-Value Target, just specifying that it is a Rendertarget, more info : https://cgcookie.com/questions/8160-after-watching-the-video-there-were-a-few-points-i-wanted-to-clarify-if-that-was-ok
                fixed4 frag(v2f IN) : SV_Target
                {
                    float4 texColor = tex2D(_MainTex,IN.uv);
                    return texColor * _Color;
                }
                
            ENDCG
        }    
    }
    
}
